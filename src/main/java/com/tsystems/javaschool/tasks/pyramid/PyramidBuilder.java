package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (inputNumbers == null || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        double el = (Math.sqrt(8 * inputNumbers.size() + 1) - 1) / 2;
        if (!(el % 1 == 0)) {
            throw new CannotBuildPyramidException();
        }

        int n = (int) el;
        int[][] array = new int[n][2 * n - 1];
        Collections.sort(inputNumbers);

        Iterator<Integer> inputIterator = inputNumbers.iterator();
        for (int i = 0; i < n; i++) {
            int pos = n - i - 1;
            for (int j = 0; j < i + 1; j++) {
                array[i][pos] = inputIterator.next();
                pos += 2;
            }
        }

        return array;
    }
}
package com.tsystems.javaschool.tasks.calculator;


public class Calculator {
    public String evaluate(String statement) {
        Evaluator evaluator = new Evaluator(statement);
        try {
            return evaluator.evaluate();
        } catch (Exception e) {
            return null;
        }
    }

    private static class Evaluator {
        private static final int N = 1000;

        private final String statement;
        private final String[] operationsStack = new String[N];
        private final String[] valuesStack = new String[N];
        private final String[] expression = new String[N];
        private int expressionPtr;
        private int operationsPtr;
        private int valuesPtr;

        public Evaluator(String statement) {
            this.statement = statement;
        }

        public String evaluate() {
            parse(statement);
            for (int i = 0; i <= expressionPtr; i++) {
                String currentChar = expression[i];
                switch (currentChar) {
                    case "(":
                        pushOperation(currentChar);
                        break;
                    case "+":
                    case "-":
                    case "*":
                    case "/":
                        if (isEmptyOperation()) {
                            pushOperation(currentChar);
                            break;
                        }
                        String operation = peekOperation();
                        if (getPriority(currentChar) <= getPriority(operation)) {
                            execute();
                        }
                        pushOperation(currentChar);
                        break;
                    case ")":
                        while (true) {
                            operation = peekOperation();
                            if (operation.equals("(")) {
                                popOperation();
                                break;
                            }
                            execute();
                        }
                        break;
                    default:
                        pushValue(currentChar);
                }
            }

            while (!isEmptyOperation()) {
                execute();
            }

            return popValue();
        }

        private void parse(String statement) {
            StringBuilder temp = new StringBuilder();
            for (int i = 0; i < statement.length(); i++) {
                char currentChar = statement.charAt(i);
                switch (currentChar) {
                    case '+':
                    case '-':
                    case '*':
                    case '/':
                    case '(':
                    case ')':
                        if (temp.length() > 0) {
                            expression[expressionPtr++] = temp.toString();
                            temp = new StringBuilder();
                        }
                        expression[expressionPtr++] = "" + currentChar;
                        break;
                    case ' ':
                        break;
                    default:
                        temp.append(currentChar);
                }
            }
            if (temp.length() > 0) {
                expression[expressionPtr] = temp.toString();
            }
        }

        private void execute() {
            String value2 = popValue();
            String value1 = popValue();
            String operation = popOperation();
            double number1 = Double.parseDouble(value1);
            double number2 = Double.parseDouble(value2);
            double result = 0.0;

            switch (operation) {
                case "+":
                    result = number1 + number2;
                    break;
                case "-":
                    result = number1 - number2;
                    break;
                case "*":
                    result = number1 * number2;
                    break;
                case "/":
                    result = number1 / number2;
                    break;
            }

            pushValue(fmt(result));
        }

        private void pushOperation(String op) {
            operationsStack[operationsPtr++] = op;
        }

        private boolean isEmptyOperation() {
            return operationsPtr == 0;
        }

        private String peekOperation() {
            return operationsStack[operationsPtr - 1];
        }

        private String popOperation() {
            return operationsStack[--operationsPtr];
        }

        private void pushValue(String v) {
            valuesStack[valuesPtr++] = v;
        }

        private String popValue() {
            return valuesStack[--valuesPtr];
        }

        private int getPriority(String operation) {
            int priority = -1;
            switch (operation) {
                case "(":
                    priority = 0;
                    break;
                case "+":
                case "-":
                    priority = 1;
                    break;
                case "*":
                case "/":
                    priority = 2;
                    break;
            }

            return priority;
        }

        private static String fmt(double d) {
            if (d == (long) d) {
                return String.format("%d", (long) d);
            } else {
                return String.format("%s", d);
            }
        }
    }
}

package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("Sequences must not be null");
        }
        // empty sequence is subsequence of anything
        if (x.isEmpty()) {
            return true;
        }

        Iterator subsequenceIterator = x.iterator();
        Iterator sequenceIterator = y.iterator();
        boolean possible = sequenceIterator.hasNext();
        while (possible && subsequenceIterator.hasNext()) {
            Object subsequenceNext = subsequenceIterator.next();
            while (possible && !sequenceIterator.next().equals(subsequenceNext)) {
                possible = sequenceIterator.hasNext();
            }
        }

        return possible;
    }
}

